Provisioning a new site
=======================

## Required packages:

* nginx
* Python 3.6
* virtualenv + pip
* git

On Ubuntu 18.04:

    sudo apt update
    sudo apt install nginx git python36 python3.6-venv

## Nginx Virtual Host config

* see nginx.template.conf
* repalce DOMAIN with site url, e.g., staging.my-domain.com

## Systemd service

* see gunicorn-systemd.template.service
* replace DOMAIN with site url, e.g., staging.my-domain.com

## Folder structure
/home/account
└── sites
    ├── DOMAIN1
    │   ├── .env
    │   ├── db.sqlite3
    │   ├── manage.py
    │   ├── static
    │   ├── virtualenv
    └── DOMAIN2
        ├── .env
        ├── db.sqlite3
        ├── manage.py
        └── (etc)

## Fabfile command
`fab -i ~/.ssh/gcp_ioglyph deploy:host=account@todo-staging.ioglyph.com`

Might need to restart gunicorn on the server -
`sudo systemctl restart gunicorn-todo-staging.ioglyph.com`

Execute the functional tests:

`STAGING_SERVER=todo-staging.ioglyph.com python manage.py test functional_tests`

Run the same for todo.ioglyph.com

Logs
`sudo journalctl -f -u gunicorn-todo-staging.ioglyph.com`

## Tagging
```
git tag -f LIVE
export TAG=`date +DEPLOYED-%F/%H%M`
git tag $TAG
git push -f origin LIVE $TAG
```
